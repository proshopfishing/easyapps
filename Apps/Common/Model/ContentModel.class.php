<?php
namespace Common\Model;
use Think\Model;
class ContentModel extends CommonModel {
    public function ckvo($vo='')
    {
        if(!$vo) return;
        if ($vo['status'] == 0) 
            $vo['status_txt'] = '未发布';
        elseif($vo['status'] == 1)
            $vo['status_txt'] = '已发布';
        $vo['ctime'] = date('Y-m-d',$vo['ctime']);
        return $vo;
    }
}    